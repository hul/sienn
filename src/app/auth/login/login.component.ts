import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { AuthService } from '../auth.service';

import { BehaviorSubject } from 'rxjs/Rx';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  inProgress = new BehaviorSubject<boolean>(false);
  errorMessage = new BehaviorSubject<string>('');

  constructor(
    private formBuilder: FormBuilder, 
    private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      user: ['', [Validators.required, Validators.minLength(3)]],
      password: ['', [Validators.required, Validators.minLength(3)]]
    });
  }

  onSubmit() {
    if (this.form.valid) {
      this.inProgress.next(true);

      const { user, password } = this.form.value;
      this.authService.login(user, password)
        .subscribe(this.onLoginSuccess.bind(this), this.onLoginFailure.bind(this));
    }
  }

  get user() { return this.form.get('user'); }
  
  get password() { return this.form.get('password'); }

  get isLoggedIn() { return this.authService.isLoggedIn(); }

  private onLoginSuccess() {  
    this.inProgress.next(false);
  }

  private onLoginFailure(error) {
    this.inProgress.next(false);
    this.errorMessage.next(error.error);
  }
}
