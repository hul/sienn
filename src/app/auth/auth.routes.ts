import { LoginComponent } from './login/login.component';

export const authRoutes = [
	{ 
		path: 'login', 
		component: LoginComponent
	},
];
