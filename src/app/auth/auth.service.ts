import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URLSearchParams } from "@angular/http"
import { Router } from '@angular/router';

import { Observable, BehaviorSubject } from 'rxjs/Rx';
import * as decode from 'jwt-decode';

const ACCESS_TOKEN_KEY = 'access_token';

const urls = {
  login: 'http://recruits.siennsoft.com/api/Jwt'
}

@Injectable()
export class AuthService {

  constructor(private http: HttpClient, private router: Router) {}

  login(user: string, password: string) {
    const httpOptions = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };

    const params = new URLSearchParams();
    params.append('UserName', user);
    params.append('Password', password);
    
    return this.http.post(urls.login, params.toString(), httpOptions)
      .do(this.onSuccess.bind(this))
      .shareReplay()
  }

  isLoggedIn() {
    const token = localStorage.getItem(ACCESS_TOKEN_KEY);
    if (token) {
      const session = decode(token);
      const isExpired = new Date().getTime() < session.exp * 1000;
      return isExpired;  
    }
    return false;
  }

  getToken() {
    return localStorage.getItem(ACCESS_TOKEN_KEY);
  }

  private onSuccess(response) {
    const { access_token, expires_in } = response;
    localStorage.setItem(ACCESS_TOKEN_KEY, access_token);

    this.router.navigate(['products']);
  }
}
