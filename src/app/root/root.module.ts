import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RootComponent } from '../root/root.component';
import { AuthModule, authRoutes } from '../auth';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';

const appRoutes: Routes = [
  ...authRoutes,
  { path: 'products', loadChildren: 'app/products/products.module#ProductsModule' },
  { path: '', redirectTo: '/products', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  declarations: [
    RootComponent,
    PageNotFoundComponent,
  ],
  imports: [
    AuthModule,
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [],
  bootstrap: [
    RootComponent
  ]
})
export class RootModule {}
