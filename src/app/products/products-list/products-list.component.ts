import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../products.types';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductsListComponent implements OnInit {
  products: Product[] = this.activatedRoute.snapshot.data.products;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
  }

}
