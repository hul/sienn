import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

import { Observable, Subject } from 'rxjs/Rx';
import { ProductsService } from './products.service';
import { Product } from './products.types';

@Injectable()
export class ProductsResolver implements Resolve<Observable<any[]>> {
  products: Subject<Product[]>;

  constructor(private productsService: ProductsService) {}
  
  resolve() {
    this.products = new Subject<Product[]>();
    this.productsService.get()
      .subscribe(this.onSuccess.bind(this), this.onFailure.bind(this));
    return this.products;
  }

  private onSuccess(response: Product[]) {
    this.products.next(response);
    this.products.complete();
  }
  
  private onFailure(errorResponse) {
    const { status, statusText } = errorResponse;
    alert(`${status} ${statusText}`);
  }
}