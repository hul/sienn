export interface Product {
	productID: Number;
	name: string;
	price: Number;
	description: String;
};
