import { NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductsListComponent } from './products-list/products-list.component';
import { LoggedUserGuard } from '../auth/logged-user.guard';
import { ProductsResolver } from './products.resolver';

const routes: Routes = [
	{ 
		path: '', 
		component: ProductsListComponent,
		canActivate: [ LoggedUserGuard ],
		resolve: { products: ProductsResolver }
	},
];

@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [
		RouterModule
	]
})
export class ProductsRoutesModule {}