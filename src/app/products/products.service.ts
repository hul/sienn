import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs/Rx';

import { Product } from './products.types';

const urls = {
  get: 'http://recruits.siennsoft.com/api/Products'
}

@Injectable()
export class ProductsService {
  constructor(private http: HttpClient) {}

  get(): Observable<Product[]> {
    return this.http.get<Product[]>(urls.get);
  }

}
