import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsListComponent } from './products-list';
import { ProductsService } from './products.service';
import { ProductsRoutesModule } from './products-routes.module';
import { ProductsResolver } from './products.resolver';

@NgModule({
	declarations: [
    ProductsListComponent
  ],
	imports: [
    CommonModule,
    ProductsRoutesModule
	],
	providers: [
    ProductsService, ProductsResolver
	]
})
export class ProductsModule {}